#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 15:01:15 2018

@author: Hai Nguyen (xielab)
"""
import os
import csv
import sys
import re
import logging
from os import listdir
from os.path import isfile,join, exists, dirname, basename, abspath, isdir
from Bio import SeqIO


lower_bound=-50
upper_bound=+2
repa_lower_bound=-10
repa_upper_bound=-3

save_path = '/media/xielab101/GAO_JIAJIA_DATA/Output'
completeName1 = os.path.join(save_path,"Intron_Metazoa_Release88_3Prime_Errors.txt")
out_file1=open(completeName1,'w')
csv_writer = csv.writer(out_file1,delimiter='\t')
completeName = os.path.join(save_path,"Intron_Metazoa_Release88_3Prime.txt")
out_file=open(completeName,'w')
csv_writer = csv.writer(out_file,delimiter='\t')

def is_valid(dna):
    if ('N' not in dna) and ('W' not in dna) and ('M' not in dna) and ('R' not in dna) and ('Y' not in dna) and ('B' not in dna) and \
    ('n' not in dna) and ('K' not in dna) and ('S' not in dna) and ('V' not in dna) and ('D' not in dna) and \
    ((dna[48] == 'A' and dna[49] == 'G') or (dna[48] == 'a' and dna[49] == 'g')):
        return 1
    else:
        return 0
   
def extract_seq(f,seq_list):
    
    genebank_file = SeqIO.parse(f, "genbank")
    fwd_strand = 0
    rev_strand = 0
    for gb_record in genebank_file:
        feature_list=[]
        for gb_feature in gb_record.features:            
            if gb_feature.type=='gene' or gb_feature.type=='mRNA':
                continue
            INTRON=False
            num_sub_feature=len(gb_feature.sub_features)
            if num_sub_feature:#if it has 'join'
                INTRON=True
                #print gb_feature.sub_features
            else:
                #judge by qualifiers
                for qualifier in gb_feature.qualifiers:
                    for item in gb_feature.qualifiers[qualifier]:
                        if item.find('intron')>=0:
                            INTRON=True
                            break
                    if INTRON:
                        break
                #consider special instances
                if not INTRON:
                    if gb_feature.type=='intron':
                        INTRON=True
            #now INTRON reflects the feature is a intron or not

            if not INTRON:
                continue
            else:
                if num_sub_feature:#if it has more than one range, pick middle range for each pair
                    pos_list=[]
                    for sub_feature in gb_feature.sub_features:
                        sub_start=sub_feature.location.nofuzzy_start
                        sub_end=sub_feature.location.nofuzzy_end
                        pos_list.append(sub_start)
                        pos_list.append(sub_end)
                    pos_list.pop(0)
                    pos_list.pop(-1)
                    strand= gb_feature.location.strand
                    for i in range(num_sub_feature-1):
                        start=pos_list.pop(0)
                        end=pos_list.pop(0)
                        feature_list.append([start,end,strand])
                    
                else:
                    start = gb_feature.location.nofuzzy_start
                    end = gb_feature.location.nofuzzy_end
                    strand= gb_feature.location.strand
                    feature_list.append([start,end,strand])
        #print feature_list

        unique_feature_list=[]
        for feature in feature_list:
            if feature not in unique_feature_list:
               unique_feature_list.append(feature)
        seq_list_temp=[]   
        for feature in unique_feature_list:
            if str(feature[2]) !='None':
               if int(feature[2]) > 0 and int(feature[0]) > int(feature[1]):
                  fwd_strand +=1
               if int(feature[2]) < 0 and int(feature[0]) < int(feature[1]):  
                  rev_strand +=1

        
        for feature in unique_feature_list:
            if str(feature[2]) =='None':
                out_file1.write(f +'\t'+'one of record has strand is None\n')
                seq_list_temp=[]
                break
            else:
                strand=int(feature[2])
                if strand>0:
                   end=int(feature[1])
                   seq_list_temp.append(gb_record.seq[end+lower_bound:end+upper_bound])
                else:
                   start=int(feature[0])
                   seq_list_temp.append(gb_record.seq[start-upper_bound:start-lower_bound].reverse_complement())
            #print seq_list_temp
            
        if len(seq_list_temp) > 0:
           for feature_seq in seq_list_temp: # Remove seq which has characters are N, n, Y and position -2 is not A and postion -1 is not G
               
               if len(feature_seq) < 52:
                  #print 'Sequence has length  < 52 ' + feature_seq # to see how many sequence which has lenght less than 52 
              
                  out_file1.write(f.split('/').pop()+' has sequence with length < 52: ')
                  out_file1.write(str(feature_seq) + '\n')
               else:           
                   if is_valid(feature_seq)==1:
                      seq_list.append(feature_seq)
    if fwd_strand > 0:
       out_file1.write(f.split('/').pop() + ' has start > end in forward strand (number of feature) ' + str(fwd_strand) + '\n')
    if rev_strand > 0:
       out_file1.write(f.split('/').pop() + ' has start < end in reverse strand (number of feature) ' + str(rev_strand) + '\n')
                            
def intron_repa_report(seq_list,dir_name):
    if len(seq_list)==0:
        print "-----------------No intron found in the reference " + dir_name
        out_file.write(dir_name + '\t' + 'No intron found in the reference \n')
        
    else:    
        num_pos=len(seq_list[0])
  
        A=[0 for j in range(num_pos)]
        T=[0 for j in range(num_pos)]
        G=[0 for j in range(num_pos)]
        C=[0 for j in range(num_pos)]
        N=[0 for j in range(num_pos)]
        ATGC=[0 for j in range(num_pos)]
        AG_percentage=[0 for j in range(num_pos)]
        A_percentage=[0 for j in range(num_pos)]
        G_percentage=[0 for j in range(num_pos)]
        T_percentage=[0 for j in range(num_pos)]
        C_percentage=[0 for j in range(num_pos)]
    
        if len(seq_list):#change ag_seq_list to seg_list if just want intron(3 places need to be changed) 
            for pos in range(num_pos):
                for feature_seq in seq_list:#change ag_seq_list to seg_list if just want intron 
                    letter=feature_seq[pos]
                    if letter=='A'or letter=='a':
                        A[pos]+=1
                    elif letter=='T'or letter=='t':
                        T[pos]+=1
                    elif letter=='G'or letter=='g':
                        G[pos]+=1
                    elif letter=='C'or letter=='c':
                        C[pos]+=1
                    else:
                       print 'Sequence has strange chracter: ' + feature_seq # Print seq which has strange characters (not A, C, G, T)
                       out_file1.write(dir_name +'/t'+ 'has sequence with strange character:\n')
                       out_file1.write(str(feature_seq) + '\n')
                       N[pos]+=1
            for pos in range(num_pos):
                    
                ATGC[pos]=A[pos]+G[pos]+T[pos]+C[pos]
                AG_percentage[pos]='{percent:.2%}'.format(percent=float(A[pos]+G[pos])/float(ATGC[pos]+N[pos]))
                A_percentage[pos]='{percent:.2%}'.format(percent=float(A[pos])/float(ATGC[pos]+N[pos]))
                G_percentage[pos]='{percent:.2%}'.format(percent=float(G[pos])/float(ATGC[pos]+N[pos]))
                T_percentage[pos]='{percent:.2%}'.format(percent=float(T[pos])/float(ATGC[pos]+N[pos]))
                C_percentage[pos]='{percent:.2%}'.format(percent=float(C[pos])/float(ATGC[pos]+N[pos]))
        else:
             print "No repa region has A/G>4."
             sys.exit(2)         
               
            
        title=[]
        if upper_bound>0 and lower_bound<0:
            num_title=upper_bound-lower_bound
        else:
            num_title=abs(lower_bound-upper_bound)+1
        for i in range(num_title+1):
            pos=lower_bound+i
            if pos!=0:
               title.append(pos)

        out_file.write(dir_name + '\n')
        csv_writer.writerow(['Pos:\t']+title)
            #for feature_seq in seq_list:#change ag_seq_list to seg_list if just want intron
                #csv_writer.writerow('\t'+feature_seq)
        csv_writer.writerow(['Total Nucleotide\t']+ ATGC)
        csv_writer.writerow(['%A/G:\t']+ AG_percentage)
        csv_writer.writerow(['%A:\t']+ A_percentage)
        csv_writer.writerow(['%C:\t']+ C_percentage)
        csv_writer.writerow(['%G:\t']+ G_percentage)
        csv_writer.writerow(['%T:\t']+ T_percentage)
            
        total_N=0
        for n in N:
            total_N+=int(n)
        if total_N:
           out_file.write('N detected\t')
           csv_writer.writerow(N)

def find_matched_files(input_paths,pattern,file_format='tsv',path_sep=',',folder_ignored=None):
    """Find files under given directories whose names contain given pattern. If
    input_paths inlcudes paths directing directly to files, then these files are added
    into output without the necessity of matching given pattern.
    """
    logger=logging.getLogger(__name__)       
    p=re.escape(pattern)
    if p!=pattern:
        logger.debug("pattern inlcudes regular expression metacharacters, escaped pattern is {}".format(p))
    matched_files=[]
    paths=map(abspath,input_paths.split(path_sep))
    if folder_ignored is None: 
        folder_ignored=[]
    ol=len(paths)
    while(len(paths)):
        path=paths.pop(0)
        if isfile(path) and (ol>0 or re.search('^.*'+p+'.*'+file_format+'$',basename(path))):
            matched_files.append(path)            
        if isdir(path) and (basename(path) not in folder_ignored):
            paths.extend(map(lambda x: join(path,x),listdir(path)))  
        ol-=1
    logger.info('Files match with the identifier "{}": {}'.format(pattern,'\n'.join(matched_files)))
    return matched_files

        
def main():
    analyzed_species = 0
    no_intron_found = 0
    total_species=0
    base_path='/media/xielab101/GAO_JIAJIA_DATA/Data_Genome/Metazoa_Release88/' # parent Folder contains files of spiceces
    list_dir = os.listdir(base_path)
    sorted_list_dir = sorted(list_dir)
    for dir_name in sorted_list_dir:
        total_species+=1
        print 'Species: ' + dir_name
        full_directory = os.path.join(base_path,dir_name)
        matched_files =find_matched_files(full_directory,'.dat',file_format='',folder_ignored=['files'])
        #sorted_files = sorted(matched_files)
        seq_list=[] # Store seq of each spieces
        for f in matched_files:
            print f
            extract_seq(f,seq_list)
        if len(seq_list)==0:
           no_intron_found+=1
        else:
            analyzed_species+=1
        intron_repa_report(seq_list,dir_name)
    print 'Total species: ',  total_species   
    print 'Total Analyzed species: ', analyzed_species
    print 'Total Species has no intron found:' , no_intron_found
    out_file.close()
    out_file1.close()
    print ('Done')

if __name__=='__main__':
    main()
